from django.urls import path
from .views import index,add_note,card


urlpatterns = [
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views
    path('add-note',add_note),
    path('card',card)
]
