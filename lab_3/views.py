from django.shortcuts import render
from .forms import FriendForm

from lab_1.models import Friend
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect

@login_required(login_url='/admin/login/')

def index(request):
    friends = Friend.objects.all() # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)
    
@login_required(login_url='/admin/login/')
def add_friend(request):
    context = {}
    form = FriendForm(request.POST or None)
    if(request.method == 'POST') and (form.is_valid()):    
        form.save()
        return HttpResponseRedirect('/lab-3')
        
    context['form'] = form
    return render(request,'lab3_form.html',context)