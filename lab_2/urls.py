from django.urls import path
from .views import index,xml,json
from .models import Note;

urlpatterns = [
    path('', index, name='index'),
    path('xml',xml),
    path('json',json)
]
