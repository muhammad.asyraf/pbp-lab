from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    notes = Note.objects.all() # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context = {}
    form = NoteForm(request.POST or None)
    if(request.method == 'POST') and (form.is_valid()):    
        form.save()
        return HttpResponseRedirect('/lab-4')
        
    context['form'] = form
    return render(request,'lab4_form.html',context)

def card(request):
    notes = Note.objects.all() # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab4_card.html', response)