### 1. Perbedaan antara JSON dan XML :
- JSON tidak menggunakan tag akhir, sedangkan XML menggunakan tag awal dan akhir.
- JSON lebih cepat untuk membaca dan menulis karena dapat diurai lebih mudah daripada XML
- JSON dapat diuraikan oleh salah satu bahasa JavaScript, XML diuraikan menggunakan parser XML.

referensi : https://www.cs.montana.edu/izurieta/pubs/caine2009.pdf

### 2. Perbedaan antara HTML dan XML :
- HTML adalah Hypertext Markup Language yang membantu mengembangkan struktur halaman web sedangkan XML adalah Extensible Markup Language yang membantu untuk bertukar data antara platform yang berbeda.
- Pada HTML beberapa tag tidak memiliki tag penutup, sementara XML wajib untuk menutup setiap tag yang digunakan.
-  HTML tidak peka huruf besar atau kecil sedangkan XML peka huruf besar atau kecil

referensi : https://perbedaan.budisma.net/perbedaan-html-dan-xml.html

