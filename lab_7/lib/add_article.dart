import 'package:flutter/material.dart';
import 'package:lab_7/main.dart';

class BelajarForm extends StatefulWidget {
  @override
  Add_Article createState() => Add_Article();
}

class Add_Article extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
   Widget build(BuildContext context){
     return Scaffold(
       backgroundColor: Color(0xff131313),
       appBar: AppBar(
          title: Text('Article'),
          backgroundColor: Color(0xff131313),
        ),
        body: Form(
              key: _formKey,
              child:
          ListView(
          padding: EdgeInsets.all(16),
          children: [
            Container(
              child: 
              Text('Add New Article',textAlign: TextAlign.center,
              style: 
              TextStyle(fontSize: 40,
              fontWeight: FontWeight.bold),
              ),
              margin: EdgeInsets.fromLTRB(0, 25, 0, 20),
            ),
            Container(
              child: 
              Text('Authors Name',
              style: 
              TextStyle(fontSize: 22,
              fontWeight: FontWeight.bold),
              ),
              margin: EdgeInsets.fromLTRB(0, 25, 0, 10),
            ),
            Container(
              child: TextFormField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    labelText: 'Authors Name'),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'nama tidak boleh kosong';
                      }
                      return null;
                    },
              ),
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
            Container(
              child: 
              Text('Title',
              style: 
              TextStyle(fontSize: 22,
              fontWeight: FontWeight.bold),
              ),
              margin: EdgeInsets.fromLTRB(0, 15, 0, 10),
            ),
            Container(
              child: TextFormField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    labelText: 'Title'),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'judul tidak boleh kosong';
                      }
                      return null;
                    },
              ),
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
            Container(
              child: 
              Text('Article Content',
              style: 
              TextStyle(fontSize: 22,
              fontWeight: FontWeight.bold),
              ),
              margin: EdgeInsets.fromLTRB(0, 15, 0, 10),
            ),
            Container(
              
              child: TextFormField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    labelText: 'Content'),
                    minLines: 14,
                    maxLines: null,
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'konten tidak boleh kosong';
                      }
                      return null;
                    },
              ),
              margin: EdgeInsets.fromLTRB(0, 15, 0, 10),
            ),
            Container(
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(color: Color(0xff6B46C1),  
              borderRadius: BorderRadius.circular(20)),  
              child: FlatButton( onPressed: () { 
                if (_formKey.currentState?.validate() ?? true) {
                  Navigator.push(context,MaterialPageRoute(builder: (_) =>MyApp()));
                }
                  
                },
                child: 
                Text( 'Submit',style: TextStyle(color: Colors.white, 
                fontSize: 25),
                ), 
                ),    
              margin: EdgeInsets.fromLTRB(0, 30, 0, 10),
            ),
          ]
        )
        )
     );
  }    
}