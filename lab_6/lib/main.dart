import 'package:flutter/material.dart';
import 'package:lab_6/add_article.dart';
import 'package:lab_6/detail.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static final String title = 'Covid Consult';

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        darkTheme: ThemeData(brightness: Brightness.dark,
            primarySwatch: Colors.purple),
        themeMode: ThemeMode.dark, 
        title: title,
        home: MainPage(title: title),
      );
}

class MainPage extends StatefulWidget {
  final String title;

  const MainPage({
    @required this.title,
  });

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          backgroundColor: Color(0xff131313),
        ),
        body: ListView(
          padding: EdgeInsets.all(16),
          children: [
            Container(
              child: 
              Text('Article',textAlign: TextAlign.center,
              style: 
              TextStyle(fontSize: 40,
              fontWeight: FontWeight.bold),
              ),
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
            Column(
            children: <Widget>[
            SizedBox(height:20.0),
            ExpansionTile(
              title: Text(
                "What is an article",
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold
                ),
              ),
              children: <Widget>[
                ExpansionTile(
                  title: Text(
                    'Article Function',
                    style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold
                ),
                  ),
                  children: <Widget>[
                    ListTile(
                      title: Text(' An article is an essay that contains facts, opinions, or ideas that are made to be published in online media and conventional media. Articles must be written in a structured and systematic manner so that they are easy to read and understand.'),
                    )
                  ],
                ),
                ListTile(
                  title: Text(
                    'As a means to convey the authors ideas, Train to think systematically, Understand the purpose of writing, and As a means of publishing the results of scientific thoughts.'
                  ),
                )
              ],
            ),
          ],
        ),
            Container(
              child: 
              Text('Search Article',textAlign: TextAlign.center,
              style: 
                TextStyle(fontSize: 30,
                ),
              ),
              margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            Container(
              child: 
              Text('Gain more insights, tips about well-being,increase your idea and write yours.',textAlign: TextAlign.center,
              style: 
                TextStyle(fontSize: 20,
                ),
              ),
              margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            Container(
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    labelText: 'Search'),
              ),
              margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            Container(
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(color: Color(0xff6B46C1),
              borderRadius: BorderRadius.circular(20)),  
              child: FlatButton( 
                onPressed: () { 
                Navigator.push(context,MaterialPageRoute(builder: (_) =>Add_Article()));  
                },
                child: 
                Text( 'Add New Article',style: TextStyle(color: Colors.white, 
                fontSize: 25),
                ), 
                ),    
              margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
            ),
            Container(
              child: buildColoredCard(),
              margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
          ),         
        ],
        ),
        bottomNavigationBar: BottomNavigationBar(
        
        backgroundColor: Color(0xff131313),
        unselectedItemColor: Colors.white,
        selectedItemColor: Colors.white,
        
        
        // type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.article),
            title: Text('Artikel'),
          ),
          
        ],
      ),
      );
      
      Widget buildSearch() {
        return const MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        ); 
      }
      
      Widget buildColoredCard() => Card(
        shadowColor: Colors.red,
        elevation: 8,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Color.fromRGBO(13, 13, 13, 1), Color(0xff131313)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'WHO issues emergency',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 20),
              Text(
                'Today, the World Health Organization (WHO) ...',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                ),
              ),
              
              ButtonBar(
              alignment: MainAxisAlignment.start,
              children: [
                FlatButton(
                  onPressed: (){Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Details()),
                    );}, 
                  child: Text(
                  'Readmore →',
                  ),
                ),
              ],
             )
            ],
          ),
        ),
      );
}

