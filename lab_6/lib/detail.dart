import 'package:flutter/material.dart';


class Details extends StatelessWidget {
   Widget build(BuildContext context){
     return Scaffold(
       backgroundColor: Color(0xff131313),
       appBar: AppBar(
          title: Text('Article'),
          backgroundColor: Color(0xff131313),
        ),
        body: ListView(
          padding: EdgeInsets.all(16),
          children: [
            Container(
              child: 
              Text('WHO issues emergency',textAlign: TextAlign.center,
              style: 
              TextStyle(fontSize: 35,
              fontWeight: FontWeight.bold),
              ),
              margin: EdgeInsets.fromLTRB(0, 25, 0, 20),
            ),
            Container(
              child: 
              Text('Post on 15 November 2021',
              textAlign: TextAlign.right,
              style: 
              TextStyle(fontSize: 20,
              ),
              ),
              margin: EdgeInsets.fromLTRB(0, 10, 0, 15),
            ),
            Container(
              child: 
              Text('Today, the World Health Organization (WHO) issued an emergency use listing (EUL) for COVAXIN® (developed by Bharat Biotech), adding to a growing portfolio of vaccines validated by WHO for the prevention of COVID-19 caused by SARS-CoV-2. WHO’s EUL procedure assesses the quality, safety and efficacy of COVID-19 vaccines and is a prerequisite for COVAX vaccine supply. It also allows countries to expedite their own regulatory approval to import and administer COVID-19 vaccines. “This emergency use listing expands the availability of vaccines, the most effective medical tools we have to end the pandemic,” said Dr Mariângela Simão, WHO Assistant-Director General for Access to Medicines and Health Products. ‘But we must keep up the pressure to meet the needs of all populations, giving priority to the at-risk groups who are still waiting for their first dose, before we can start declaring victory.” COVAXIN® was assessed under the WHO EUL procedure based on the review of data on quality, safety, efficacy, a risk management plan and programmatic suitability. The Technical Advisory Group (TAG), convened by WHO and made up of regulatory experts from around the world, has determined that the vaccine meets WHO standards for protection against COVID-19, that the benefit of the vaccine far outweighs risks and the vaccine can be used globally. The vaccine is formulated from an inactivated SARS-CoV-2 antigen and is presented in single dose vials and multidose vials of 5, 10 and 20 doses',
              textAlign: TextAlign.justify,
              style: 
              TextStyle(fontSize: 20,
              ),
              ),
              margin: EdgeInsets.fromLTRB(10, 20, 10, 10),
            ),
            Container(
              child: 
              Text('Made by WHO',
              textAlign: TextAlign.center,
              style: 
              TextStyle(fontSize: 20,
              fontWeight: FontWeight.bold
              ),
              ),
              margin: EdgeInsets.fromLTRB(0, 20, 0, 10),
            ),
            ])
       );
   }
}
